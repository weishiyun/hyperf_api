<?php

declare(strict_types=1);

use function Hyperf\Support\env;

/**
 * JWT配置
 */
return [
    'default' => [
        'scene' => '',
        // 登录方式，sso为单点登录，mpop为多点登录
        'login_type' => env('JWT_LOGIN_TYPE', 'sso'), 
        // 单点登录自定义数据中必须存在uid的键值，这个key你可以自行定义，只要自定义数据中存在该键即可
        'sso_uid' => 'uid',
        // 非对称加密使用字符串,请使用自己加密的字符串
        'secret' => env('JWT_SECRET', 'ZXGJHZfb9EvcCfVpHo5u93BzQj3I6B7n'), 
        /**
         * JWT 权限keys
         * 对称算法: HS256, HS384 & HS512 使用 `JWT_SECRET`.
         * 非对称算法: RS256, RS384 & RS512 / ES256, ES384 & ES512 使用下面的公钥私钥.
         */
        'keys' => [
            'public' => env('JWT_PUBLIC_KEY'), // 公钥，例如：'file://path/to/public/key'
            'private' => env('JWT_PRIVATE_KEY'), // 私钥，例如：'file://path/to/private/key'
        ],
        // token过期时间，单位为秒
        'ttl' => env('JWT_TTL', 7200), 
        // jwt的hearder加密算法
        'alg' => env('JWT_ALG', 'HS256'), 
        // 宽限时间 单位为：秒，注意：如果使用单点登录，该宽限时间无效
        'grace_period' => env('JWT_BLACKLIST_GRACE_PERIOD', 60)
    ],
    /**
     * 不同场景的token，scene必须存在一个default
     */
    'scene' => [
        'api' => [
            'secret' => env('JWT_API_SECRET', 'ZXGJHZfb9EvcCfVpHo5u93BzQj3I6B7n'),
            'login_type' => 'sso',
            'ttl' => 7200,
            'blacklist_cache_ttl' => env('JWT_TTL', 7200),
        ]
    ],
];
