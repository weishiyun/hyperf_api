<?php

declare(strict_types=1);

/**
 * 中间件
 */
return [
    'http' => [
        App\Owns\Middleware\Core::class
    ],
];
