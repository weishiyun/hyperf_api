<?php

declare(strict_types=1);

namespace App\Event;

/**
 * 登录日志
 */
class LoginEvent
{
    public string $message;
    
    public function __construct(string $message)
    {
        $this->message = $message;    
    }
}