<?php

declare(strict_types=1);

namespace App\Listener;

use App\Event\LoginEvent;
use App\Owns\Enum\AppEnum;
use App\Owns\Helper\HttpRequest;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\HttpServer\Contract\RequestInterface;

use function Hyperf\Support\make;

/**
 * 接口日志、登录日志、操作日志
 */
#[Listener]
class LogListener implements ListenerInterface
{
    #[Inject]
    protected RequestInterface $request;

    public function listen(): array
    {
        return [
            LoginEvent::class
        ];
    }

    /**
     * @param LoginEvent $event
     */
    public function process(object $event): void
    {
        $this->loginLog($event);
    }
    /**
     * 登录日志
     */
    protected function loginLog(object $event)
    {
        $http = make(HttpRequest::class);
        $log = [
            'username' => $this->request->input('username', ''),
            'user_mode' => AppEnum::AUTH_MODE,
            'param' => $http->getParam(),
            'message' => $event->message,
            'status' => $event->message == '登录成功' ? 1 : 2,
            'ip' => $http->getIp(),
            'os' => $http->getOs(),
            'browser' => $http->getBrowser(),
            'created_at' => now_date(),
        ];
        Db::table('system_login_log')->insert($log);
    }
    
}
