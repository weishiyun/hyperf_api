<?php

declare(strict_types=1);

namespace App\Controller;

use App\Owns\Abstract\OwnsController;
use App\Owns\Annotation\Oper;
use App\Owns\Annotation\Record;
use App\Service\System\MaterialService;
use App\Service\TestService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;

#[Controller('index')]
class IndexController extends OwnsController
{
    #[Inject]
    protected TestService $test;

    #[GetMapping('index'), Record(), Oper()]
    public function index()
    {
        $res = 'zzz';
        // $res = $this->test->user();
        return $this->success($res);
    }

    #[PostMapping('upload')]
    public function upload()
    {
        $file = $this->request->file('file');

        $service = new MaterialService();
        $res = $service->upload($file);

        return $this->success($res, '上传成功');
    }
}
