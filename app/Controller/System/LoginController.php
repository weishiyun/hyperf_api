<?php

declare(strict_types=1);

namespace App\Controller\System;

use App\Event\LoginEvent;
use App\Owns\Abstract\OwnsController;
use App\Owns\Annotation\Auth;
use App\Owns\Exception\TipException;
use App\Owns\Library\OwnsAuth;
use App\Service\System\LoginService;
use App\Service\System\MenuService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * 用户登录
 */
#[Controller('system/login')]
class LoginController extends OwnsController
{
    #[Inject]
    protected LoginService $service;

    #[Inject]
    protected EventDispatcherInterface $event;

    /**
     * 账号登录
     */
    #[PostMapping('account')]
    public function account()
    {
        $input = $this->request->all();
        $this->validate($input, [
            'username' => 'required|between:2,20',
            'password' => 'required|between:6,32'
        ]);
        try {
            $res = $this->service->account($input);
            $this->event->dispatch(new LoginEvent('登录成功'));
            return $this->success($res);
        } catch (TipException $e) {
            $this->event->dispatch(new LoginEvent($e->getMessage()));
            return $this->error($e->getMessage());
        }
    }
    /**
     * 账号登出
     */
    #[PostMapping('logout'), Auth()]
    public function logout()
    {
        return $this->success($this->service->logout());
    }
    /**
     * 用户信息
     */
    #[GetMapping('info'), Auth()]
    public function info()
    {
        $res['user'] = $this->service->info();
        $res['codes'] = '*';
        $res['roles'] = OwnsAuth::roleCode();
        $res['routers'] = (new MenuService())->getRoutersMenu(OwnsAuth::roleIds());
        return $this->success($res);
    }
    /**
     * 刷新token
     */
    #[PostMapping('refresh'), Auth()]
    public function refresh()
    {
        $token = $this->request->getHeaders()['authorization'][0] ?? '';
        return $this->success($this->service->refresh($token));
    }
    /**
     * 背景图
     */
    #[GetMapping('backgroundImage')]
    public function backgroundImage()
    {
        return $this->success('');
    }
}
