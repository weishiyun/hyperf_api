<?php

declare(strict_types=1);

namespace App\Controller\System;

use App\Owns\Abstract\OwnsController;
use App\Service\System\MaterialService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;

/**
 * 文件上传
 */
#[Controller('system/file')]
class FileController extends OwnsController
{
    #[Inject]
    protected MaterialService $service;
    /**
     * 上传
     */
    #[PostMapping('upload')]
    public function upload()
    {
        $res = $this->service->upload($this->request->file('file'));
        return $this->success($res);
    }
    /**
     * 切片上传
     */
    #[PostMapping('chunkUpload')]
    public function chunkUpload()
    {
        $input = $this->request->inputs(['index', 'total', 'hash', 'name', 'size']);
        $this->validate($input, [
            'index' => 'required',
            'total' => 'required',
            'hash' => 'required',
            'name' => 'required',
            'size' => 'required',
        ]);
        $res = $this->service->chunkUpload($this->request->file('file'), $input);
        return $this->success($res);
    }
    /**
     * 文件信息
     */
    #[GetMapping('info')]
    public function info()
    {
        $materialId = $this->request->query('material_id', '');
        return $this->success($this->service->info($materialId));
    }
    /**
     * 下载
     */
    #[GetMapping('download')]
    public function downloads()
    {
        return $this->download($this->request->query('path', ''));
    }
}
