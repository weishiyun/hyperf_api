<?php

declare(strict_types=1);

namespace App\Owns\Exception\Handler;

use App\Owns\Enum\RespCode;
use Hyperf\Codec\Json;
use Hyperf\Context\Context;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Exception\MethodNotAllowedHttpException;
use Hyperf\HttpMessage\Exception\NotFoundHttpException;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * 路由异常处理
 */
class RouteExceptionHandle extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response): ResponseInterface
    {
        $this->stopPropagation();
        $response = Context::get(ResponseInterface::class);
        return $response->withHeader('server', 'owns')
            ->withAddedHeader('content-type', 'application/json; charset=utf-8')
            ->withStatus(404)
            ->withBody(new SwooleStream(Json::encode(RespCode::respMessage('Not Found', 404))));
    }

    public function isValid(Throwable $throwable): bool
    {
        if ($throwable instanceof NotFoundHttpException) {
            return true;
        }
        if ($throwable instanceof MethodNotAllowedHttpException) {
            return true;
        }
        return false;
    }
}
