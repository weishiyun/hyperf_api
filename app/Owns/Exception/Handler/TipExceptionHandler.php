<?php

declare(strict_types=1);

namespace App\Owns\Exception\Handler;

use App\Owns\Enum\RespCode;
use App\Owns\Exception\TipException;
use Hyperf\Codec\Json;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * 客户端异常处理
 */
class TipExceptionHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response): ResponseInterface
    {
        $this->stopPropagation();
        $respData = RespCode::respMessage($throwable, $throwable->getCode());
        return $response->withHeader('server', 'owns')
            ->withAddedHeader('content-type', 'application/json; charset=utf-8')
            ->withStatus(200)
            ->withBody(new SwooleStream(Json::encode($respData)));
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof TipException;
    }
}
