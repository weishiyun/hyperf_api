<?php

declare(strict_types=1);

namespace App\Owns\Exception;

/**
 * 客户端提示
 */
class TipException extends \RuntimeException
{
}
