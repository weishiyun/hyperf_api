<?php

declare(strict_types=1);

namespace App\Owns\Abstract;

use Hyperf\Database\Model\Model;

/**
 * 数据模型
 */
abstract class OwnsModel extends Model
{
    const DELETED_AT = 'deleted_at';

    public bool $incrementing = false;

}