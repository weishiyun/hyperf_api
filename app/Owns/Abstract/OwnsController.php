<?php

declare(strict_types=1);

namespace App\Owns\Abstract;

use App\Owns\Enum\RespCode;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Psr\Http\Message\ResponseInterface as Psr7ResponseInterface;

/**
 * 控制器
 */
abstract class OwnsController
{
    #[Inject]
    protected RequestInterface $request;

    #[Inject]
    protected ResponseInterface $response;

    #[Inject]
    protected ValidatorFactoryInterface $validator;

    /**
     * @param int|string|array|object $data
     * @param string $message
     */
    public function success($data, string $message = ''): Psr7ResponseInterface
    {
        return $this->response->json([
            'code' => RespCode::SUCCESS,
            'data' => $data,
            'message' => $message ?: 'success'
        ]);
    }
    /**
     * @param string $message
     * @param int $code
     */
    public function error(string $message, int $code = RespCode::ERROR): Psr7ResponseInterface
    {
        return $this->response->json([
            'code' => $code,
            'data' => '',
            'message' => $message ?: 'error'
        ]);
    }
    /**
     * 验证
     * @param array $data 验证数据
     * @param array $rules 验证规则
     * @param array $messages 信息提示
     * @param array $custom 自定义属性
     */
    public function validate(array $data, array $rules, $messages = [], $custom = []): void
    {
        $validator = $this->validator->make($data, $rules, $messages, $custom);
        if ($validator->fails()) {
            tips($validator->errors()->first());
        }
    }
    /**
     * 分页
     */
    public function paging(): array
    {
        $param = $this->request->all();
        $param['offset'] = (($param['page'] ?? 1) - 1) * ($param['pageSize'] ?? 10);
        $param['limit'] = (int) ($param['pageSize'] ?? 10);
        return $param;
    }
    /**
     * 下载
     */
    public function download(string $path, string $name = ''): Psr7ResponseInterface
    {
        return $this->response->download(BASE_PATH . '/public' . $path, $name);
    }
}
