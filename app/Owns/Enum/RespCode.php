<?php

declare(strict_types=1);

namespace App\Owns\Enum;

use function Hyperf\Support\env;

/**
 * 响应状态码
 */
class RespCode
{
    public const SUCCESS = 200;         // 成功
    public const ERROR = 500;           // 错误

    public const NO_USER = 1000;            // 用户不存在
    public const NO_LOGIN = 1001;           // 用户未登录
    public const NO_PERMISSION = 1002;      // 没有权限
    public const NO_DATA = 1003;            // 没有数据
    
    public const SIGN_UNBORT = 1010;        // 签名未生效
    public const SIGN_EXPIRE = 1011;        // 签名过期
    public const SIGN_MISSING = 1012;       // 签名为空
    public const SIGN_ERROR = 1013;         // 签名错误

    public const DEF_TIPS = '服务异常，请联系客服...';

    /**
     * 错误信息提示
     * @param object|string $e
     * @param integer $code
     */
    public static function getMessage($e): string
    {
        return env('APP_ENV') == 'dev' ? $e->getMessage() : self::DEF_TIPS;
    }

    /**
     * 错误信息提示
     * @param object|string $e
     * @param integer $code
     */
    public static function respMessage($e, $code = 500): array
    {
        if(is_string($e)) {
            $message = $e;
        } else {
            $message = env('APP_ENV') == 'dev' ? $e->getMessage() : self::DEF_TIPS;
        }
        return ['code' => $code, 'message' => $message, 'data' => ''];
    }

}