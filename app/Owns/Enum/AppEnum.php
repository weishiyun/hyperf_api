<?php

declare(strict_types=1);

namespace App\Owns\Enum;

/**
 * 系统常量
 */
class AppEnum
{
    // 接口请求用户标识
    public const AUTH_MARK = 'owns:u:auth_mark';
    // 系统用户端
    public const AUTH_MODE = 'u';
    // jwt
    public const AUTH_JWT = 'owns:u:auth_jwt';

    // 系统用户类型：u=系统管理员 s=商家 m=用户 j=任务端
    public const MODE_USER = 'u';
    public const MODE_STORE = 's';
    public const MODE_MEMBER = 'm';
    public const MODE_JOBS = 'j';

    // 状态
    public const ENABLE = 1;
    public const DISABLE = 0;
    // 未删除
    public const UNDELETE = 0;
    // 秘钥
    public const SECRET = '64t514m38a01876k93';
}
