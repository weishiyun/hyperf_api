<?php

/**
 * 常用函数
 */

use App\Owns\Enum\RespCode;
use App\Owns\Exception\TipException;
use Hyperf\Context\ApplicationContext;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Redis\Redis;
use Hyperf\Redis\RedisFactory;
use Hyperf\Snowflake\IdGeneratorInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

if (!function_exists('tips')) {
    /**
     * 提示异常
     */
    function tips(string $msg, int $code = RespCode::ERROR): void
    {
        throw new TipException($msg, $code);
    }
}

if (!function_exists('container')) {
    /**
     * 获取容器实例
     */
    function container(): ContainerInterface
    {
        return ApplicationContext::getContainer();
    }
}

if (!function_exists('generate')) {
    /**
     * 生成id
     */
    function generate_id(): string
    {
        return container()->get(IdGeneratorInterface::class)->generate();
    }
}

if (!function_exists('redis')) {
    /**
     * 获取Redis实例
     */
    function redis(string $name = 'default'): Redis
    {
        return container()->get(RedisFactory::class)->get($name);
    }
}

if (!function_exists('logger')) {
    /**
     * 获取日志实例
     * @param string $group
     */
    function logger(string $group = 'default'): LoggerInterface
    {
        return container()->get(LoggerFactory::class)->get('log', $group);
    }
}

if (!function_exists('app_config')) {
    /**
     * 获取配置
     * @param string $key
     */
    function app_config(string $key, $default = null)
    {
        return container()->get(ConfigInterface::class)->get($key, $default);
    }
}
if (!function_exists('format_size')) {
    /**
     * 格式化大小
     * @param int $size
     */
    function format_size(int $size): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $index = 0;
        for ($i = 0; $size >= 1024 && $i < 5; $i++) {
            $size /= 1024;
            $index = $i;
        }
        return round($size, 2) . $units[$index];
    }
}

if (!function_exists('now_date')) {
    /**
     * 日期
     */
    function now_date(int $timestamp = 0): string
    {
        if (!$timestamp) {
            $timestamp = time();
        }
        return date('Y-m-d H:i:s', $timestamp);
    }
}

if (!function_exists('list_format')) {
    /**
     * 列表格式
     */
    function list_format(array|object $list, int $total, $attach = ''): array
    {
        return [
            'list' => $list,
            'total' => $total,
            'attach' => $attach
        ];
    }
}

if (!function_exists('recursion')) {
    /**
     * 递归遍历
     * @param array $data 遍历的数组
     * @param int $index 顶级id
     * @param bool $child true没有子集不显示父级
     * @param string $pk tree键名
     * @param string $ck list键名
     */
    function recursion(array $data, $index = 0, $children = false, string $pk = 'pid', string $ck = 'id'): array
    {
        $result = [];
        foreach ($data as $item) {
            if ($index == $item[$pk]) {
                $subset = recursion($data, $item[$ck], $children, $pk, $ck);
                if ($children) {
                    if ($subset) {
                        $item['children'] = $subset;
                        $result[] = $item;
                    }
                } else {
                    $item['children'] = $subset;
                    $result[] = $item;
                }
            }
        }
        return $result;
    }
}
