<?php

declare(strict_types=1);

namespace App\Owns\Helper;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Request;

class HttpRequest
{
    #[Inject]
    protected Request $request;

    /**
     * 获取请求IP.
     */
    public function getIp(): string
    {
        $ip = $this->request->getServerParams()['remote_addr'] ?? '0.0.0.0';
        $headers = $this->request->getHeaders();
        if (isset($headers['x-real-ip'])) {
            $ip = $headers['x-real-ip'][0];
        } elseif (isset($headers['x-forwarded-for'])) {
            $ip = $headers['x-forwarded-for'][0];
        } elseif (isset($headers['http_x_forwarded_for'])) {
            $ip = $headers['http_x_forwarded_for'][0];
        }
        return $ip;
    }
    /**
     * 请求数据
     */
    public function getParam(): string
    {
        return json_encode($this->request->all(), JSON_UNESCAPED_UNICODE);
    }
    /**
     * 客户端浏览器
     */
    public function getBrowser(): string
    {
        $userAagent = $this->request->getHeaders()['user-agent'][0] ?? '';
        if (false !== stripos($userAagent, 'Edge')) {
            $browser = 'Edge';
        } elseif (false !== stripos($userAagent, 'Chrome')) {
            $browser = '谷歌';
        } elseif (false !== stripos($userAagent, 'Safari')) {
            $browser = '苹果';
        } elseif (false !== stripos($userAagent, 'Firefox')) {
            $browser = '火狐';
        } elseif (false !== stripos($userAagent, 'Opera')) {
            $browser = 'Opera';
        } else if (false !== stripos($userAagent, 'MSIE')) {
            $browser = 'IE';
        } else {
            $browser = '未知浏览器';
        }
        return $browser;
    }
    /**
     * 操作系统
     */
    public function getOs(): string
    {
        return $this->request->getHeaders()['sec-ch-ua-platform'][0] ?? '未知系统';
    }
    /**
     * 请求路由信息
     */
    public function getRoute(): array
    {
        $route = $this->request->url() ?? '';
        $module = explode('/', $route)[0] ?? '';
        $method = $this->request->getMethod() ?? '';
        return [$module, $route, $method];
    }

}
