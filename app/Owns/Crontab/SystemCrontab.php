<?php

declare(strict_types=1);

namespace App\Owns\Crontab;

use App\Owns\Library\OwnsLog;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Throwable;

#[Crontab(name: "system", rule: "1 3 1 * *", callback: "execute", memo: "system定时任务，每月月初执行")]
class SystemCrontab
{
    #[Inject]
    protected OwnsLog $log;

    public function execute()
    {
        $this->createLogTable();
    }

    /**
     * 创建系统日志表
     */
    public function createLogTable(): void
    {
        $month = date('Ym', strtotime('+1 month'));
        //日志表名
        $tableName = app_config('databases.default.prefix') . "system_log_{$month}";
        try {
            //是否已创建
            $table = Db::select("show tables like '{$tableName}'");
            if (!$table) {
                //创建表sql
                $sql = "CREATE TABLE `{$tableName}` (
                    `id` bigint NOT NULL AUTO_INCREMENT,
                    `module` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'u' COMMENT '请求路由模块',
                    `level` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'debug' COMMENT '日志等级：debug、info、warning、error',
                    `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '操作参数',
                    `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '日志信息',
                    `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注',
                    `route` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求路由',
                    `method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求方法',
                    `uid` bigint unsigned DEFAULT '0' COMMENT '操作用户id',
                    `mode` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'u' COMMENT '端口: u服务端 s商家端 m客户端 j=任务端',
                    `created_at` datetime DEFAULT NULL COMMENT '生成时间',
                    PRIMARY KEY (`id`)
                  ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='{$month}系统日志';";
                Db::select($sql);
            } else {
                $this->log->info($tableName . '日志表', '生成成功', 'system_crontab');
            }
        } catch (Throwable $e) {
            $this->log->debug($tableName . '日志表', $e->getMessage(), 'system_crontab');
        }
    }
}
