<?php

declare(strict_types=1);

namespace App\Owns\Library;

use App\Owns\Enum\AppEnum;
use App\Owns\Enum\RespCode;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Jwt验证
 */
class OwnsJwt
{
    private static $jwt;
    // jwt默认配置
    private $default;
    // 场景配置
    private $scene;

    private function __construct()
    {
        $config = app_config('jwt');
        if (!$config) {
            tips('jwt配置错误');
        }
        $this->default = $config['default'];
        $this->scene = $config['scene'];
    }
    private function __clone()
    {
    }
    public static function instance(): OwnsJwt
    {
        if (self::$jwt instanceof OwnsJwt) {
            return self::$jwt;
        }
        self::$jwt = new self();
        return self::$jwt;
    }
    /**
     * 生成token
     */
    public function getToken(array $claims, string $scene = ''): string
    {
        $config = $this->sceneConfig($scene);
        $claims['mode'] = AppEnum::AUTH_MODE;
        $only = $scene . $claims[$config['sso_uid']];
        if ($config['login_type'] == 'mpop') {
            $jti = uniqid($only, true);
        } else {
            $jti = $only;
        }
        $time = time();
        $payload = [
            'iss' => 'owns',    // jwt签发者
            'sub' => '',        // jwt所面向的用户
            'aud' => 'member',  // 接收jwt的一方
            'iat' => $time,     // jwt的签发时间
            'nbf' => $time,     // 开始生效时间
            'jti' => $jti,      // jwt的唯一身份标识，主要用来作为一次性token
            'exp' => $time + $config['ttl'], // 过期时间
            'data' => json_encode($claims, JSON_UNESCAPED_UNICODE)
        ];
        $token = $this->mapped($config, $payload, true);
        redis()->hset(AppEnum::AUTH_JWT, $jti, $token);
        return $token;
    }
    /**
     * 校验token
     */
    public function checkToken(string $token, string $scene = ''): array
    {
        $token = str_replace('Bearer ', '', $token);
        if ($token == 'null' || !$token) {
            return [];
        }
        $config = $this->sceneConfig($scene);
        $payload = $this->mapped($config, $token, false);
        $time = time();
        if ($payload->nbf > $time) {
            tips('签名未生效', RespCode::NO_PERMISSION);
        }
        if ($payload->exp < $time) {
            tips('签名已过期', RespCode::SIGN_EXPIRE);
        }
        $cache = redis()->hget(AppEnum::AUTH_JWT, $payload->jti);
        if ($token !== $cache) {
            tips('签名信息错误', RespCode::SIGN_ERROR);
        }
        return json_decode($payload->data, true);
    }
    /**
     * 刷新token
     */
    public function refreshToken(string $token, string $scene = ''): string
    {
        $config = $this->sceneConfig($scene);
        $payload = $this->mapped($config, $token, false);
        $time = time();
        $data = [
            'iss' => $payload->iss,
            'sub' => '',
            'aud' => 'member',
            'iat' => $time,
            'nbf' => $time,
            'jti' => $payload->jti,
            'exp' => $time + $config['ttl'],
            'data' => $payload->data
        ];
        $newToken = $this->mapped($config, $data, true);
        redis()->hset(AppEnum::AUTH_JWT, $payload->jti, $newToken);
        return $newToken;
    }
    /**
     * 退出
     */
    public function logout(string $uid, string $scene = ''): bool
    {
        $config = $this->sceneConfig($scene);
        if ($config['login_type'] == 'mpop') {
            $jti = uniqid($scene . $uid, true);
        } else {
            $jti = $scene . $uid;
        }
        return redis()->hdel(AppEnum::AUTH_JWT, $jti) ? true : false;
    }
    /**
     * 配置覆盖
     * @param string $key
     */
    private function sceneConfig(string $key): array
    {
        $config = $this->default;
        if ($key) {
            $scene = $this->scene[$key];
            $config['scene'] = $key;
            $config['secret'] = $scene['secret'];
            $config['login_type'] = $scene['login_type'];
            $config['ttl'] = $scene['ttl'];
            $config['blacklist_cache_ttl'] = $scene['blacklist_cache_ttl'];
        }
        return $config;
    }
    /**
     * @param boolean $operate
     * @param array|string $param
     * @param boolean $operate
     */
    private function mapped(array $config, array|string $param, bool $operate)
    {
        try {
            return match ($config['alg']) {
                'HS256', 'HS384', 'HS512' => $this->HS($config, $param, $operate),
                'ES256', 'ES384', 'ES512' => $this->ES($config, $param, $operate),
                'RS256', 'RS384', 'RS512' => $this->RS($config, $param, $operate),
            };
        } catch (ExpiredException) {
            tips('签名信息错误', RespCode::SIGN_ERROR);
        }
    }

    private function HS(array $config, array|string $param, bool $operate): string|object
    {
        if ($operate) {
            return JWT::encode($param, $config['secret'], $config['alg']);
        } else {
            return JWT::decode($param, new Key($config['secret'], $config['alg']));
        }
    }

    private function ES(array $config, array|string $param, bool $operate): string|object
    {
        if ($operate) {
            return JWT::encode($param, $config['keys']['private'], $config['alg']);
        } else {
            return JWT::decode($param, new Key($config['keys']['public'], $config['alg']));
        }
    }

    private function RS(array $config, array|string $param, bool $operate): string|object
    {
        if ($operate) {
            return JWT::encode($param, $config['keys']['private'], $config['alg']);
        } else {
            return JWT::decode($param, new Key($config['keys']['public'], $config['alg']));
        }
    }
}
