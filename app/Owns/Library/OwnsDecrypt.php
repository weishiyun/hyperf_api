<?php

declare(strict_types=1);

namespace App\Owns\Library;

/**
 * 客户端
 */
class OwnsDecrypt
{
    // 秘钥
    const SECRET_KEY = 'e05b09db6e1bc1a31k8e6c59236e111e';
    // 加密方法
    const CIPHER = 'aes-128-cbc';
    // 数据格式选项
    const OPTION = 0;
    // 密初始化向量
    const IV = 'xY4WbqB1YUmc0VUe';
    /**
     * 解密
     * @param string $data 加密字符串
     * @param string $secretKey 加密秘钥
     */
    public static function encrypt(string $data, string $secretKey = ''): string
    {
        $secretKey = $secretKey ?: self::SECRET_KEY;
        return openssl_encrypt($data, self::CIPHER, $secretKey, self::OPTION, self::IV);
    }
    /**
     * 解密
     * @param string $data 解密字符串
     * @param string $secretKey 解密秘钥
     */
    public static function decrypt(string $data, string $secretKey = ''): string
    {
        $secretKey = $secretKey ?: self::SECRET_KEY;
        return openssl_decrypt($data, self::CIPHER, $secretKey, self::OPTION, self::IV);
    }
}
