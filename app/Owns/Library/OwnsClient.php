<?php

declare(strict_types=1);

namespace App\Owns\Library;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Guzzle\CoroutineHandler;

/**
 * 客户端
 */
class OwnsClient
{
    /**
     * 初始化
     */
    private function init(string $baseUri, array $headers): Client
    {
        // if (!$headers) {
        //     $headers = [
        //         'Content-Type' => 'application/json;charset=UTF-8',
        //         'Accept' => 'application/json'
        //     ];
        // }
        $config = [
            'base_uri' => $baseUri,
            'handler' => HandlerStack::create(new CoroutineHandler()),
            'timeout' => 5,
            'swoole' => [
                'timeout' => 10,
                'socket_buffer_size' => 1024 * 1024 * 2,
            ]
        ];
        if ($headers) {
            $config['headers'] = $headers;
        }
        return (new Client($config));
    }
    /**
     * @param object $response
     */
    private function output(object $response): string
    {
        if ($response->getStatusCode() == 200) {
            return $response->getBody()->getContents();
        }
        tips($response->getReasonPhrase());
    }

    /**
     * post
     * @param string $baseUri 请求域名
     * @param string $uri 请求路径
     * @param array $param 请求参数
     * @param array $header 请求头信息
     */
    public function post(string $baseUri, string $uri, array $param, array $header = []): string
    {
        $client = $this->init($baseUri, $header);
        $response = $client->post($uri, $param);
        return $this->output($response);
    }
    /**
     * get
     */
    public function get(string $baseUri, string $uri, array $param = [], array $header = []): string
    {
        $client = $this->init($baseUri, $header);
        $response = $client->get($uri, $param);
        return $this->output($response);
    }
    /**
     * put
     */
    public function put(string $baseUri, string $uri, array $param = [], array $header = []): string
    {
        $client = $this->init($baseUri, $header);
        $response = $client->request('PUT', $uri, $param);
        return $this->output($response);
    }
    /**
     * delete
     */
    public function delete(string $baseUri, string $uri, array $param = [], array $header = []): string
    {
        $client = $this->init($baseUri, $header);
        $response = $client->request('DELETE', $uri, $param);
        return $this->output($response);
    }
}
