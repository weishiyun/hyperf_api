<?php

declare(strict_types=1);

namespace App\Owns\Library;

use App\Owns\Enum\AppEnum;
use GuzzleHttp\Stream\Stream;
use Hyperf\Context\Context;
use Hyperf\Contract\ConfigInterface;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Filesystem\FilesystemFactory;
use Hyperf\HttpMessage\Upload\UploadedFile;
use League\Flysystem\Filesystem;

/**
 * 文件处理
 */
class OwnsFile
{
    #[Inject]
    protected ConfigInterface $config;
    #[Inject]
    protected Filesystem $filesystem;
    #[Inject]
    protected FilesystemFactory $factory;

    protected string $localRoot; //本地存储路径
    protected string $appDomain; //应用域名
    protected string $storageMode; //文件存储方式

    public const IMAGE = 'image'; //图片
    public const VIDEO = 'video'; //视频
    public const AUDIO = 'audio'; //音频
    public const OTHER = 'other'; //其他文件
    public const TEMP = 'temp'; //临时文件
    public const MB = 1048576; //1MB
    public const DS = DIRECTORY_SEPARATOR;

    public function __construct()
    {
        $this->localRoot = $this->config->get('file.storage.local.root');
        $this->appDomain = $this->config->get('file.domain');
        $this->storageMode = $this->config->get('file.default');
    }

    /**
     * 文件归类
     * @param string $suffix 文件后缀名
     */
    public static function classify(string $suffix): string
    {
        return match ($suffix) {
            'jpg', 'png', 'bmp', 'jpeg', 'gif', 'webp' => self::IMAGE, //图片类文件
            'mp4', 'avi', 'mov', 'mpeg' => self::VIDEO, //视频类文件
            'mp3', 'wav' => self::AUDIO, //音频类文件
            'zip', 'rar', 'xls', 'xlsx', 'doc', 'docx', 'pdf' => self::OTHER, //其他文件
            default => self::TEMP, //临时文件
        };
    }
    /**
     * 上传文件处理
     * @param UploadedFile $file
     */
    public function handleUpload(UploadedFile $file): array
    {
        // 文件后缀名
        $suffix = $file->getExtension();
        // 文件类型
        if (self::TEMP == self::classify($suffix)) {
            tips('系统不支持.' . $suffix . '文件上传');
        }

        $mid = generate_id();
        // 原文件名
        $filename = $file->getClientFilename();
        // 保存路径
        $path = $this->getPath($suffix, $mid);

        $stream = fopen($file->getRealPath(), "r+");
        $this->filesystem->writeStream($path, $stream);
        fclose($stream);
        // 文件大小
        $size = round($file->getSize() / self::MB, 2);

        return $this->materialData($mid, $path, $suffix, $filename, $size);
    }
    /**
     * 文件流处理
     * @param Stream $stream 文件流
     * @param string $suffix 后缀名
     * @param boolean $temp 是否临时文件
     * @param boolean $oss 是否保存到oss
     */
    public function handleStream(Stream $stream, string $suffix, $temp = true, $oss = false): array
    {
        $mid = generate_id();
        $path = $this->getPath($suffix, $mid);
        $this->filesystem->writeStream($path, $stream);
        // 文件信息
        $material = [
            'path' => $path,
            'url' => $this->appDomain . $path
        ];
        if ($temp) {
            return $material;
        }
        $type = self::classify($suffix);
        if (self::IMAGE == $type) {
            list($path, $resource) = self::webpConvert($path, $suffix);
            $stream = fopen(BASE_PATH . self::DS . $resource, "r+");
            $this->filesystem->writeStream($path, $stream);
            fclose($stream);
            unlink(BASE_PATH . self::DS . $resource);
        }
        if ($this->isOss($type) || $oss) {
            $material['url'] = $this->dispatch($path);
            unlink($this->localRoot . $path);
        }
        $material = array_merge($material, [
            'material_id' => $mid,
            'storage_mode' => $this->storageMode,
            'origin_name' => '',
            'object_name' => basename($path),
            'type' => $type,
            'size' => ''
        ]);
        return $this->saveMaterial($material);
    }
    /**
     * 处理分片
     * @param UploadedFile $file
     * @param array $input
     */
    public function handleChunkUpload(UploadedFile $file, array $input): array
    {
        $chunkdir = BASE_PATH . '/public/chunk/';
        // 切片目录检测
        if (!file_exists($chunkdir) && !is_dir($chunkdir)) {
            mkdir($chunkdir, 0777, true);
        }
        $chunkName = $chunkdir . $input['hash'] . '_' . $input['index'] . '.chunk';
        // 保存切片
        $stream = fopen($file->getRealPath(), "r+");
        $this->filesystem->writeStream($chunkName, $stream);
        fclose($stream);

        $res = [];
        if ($input['index'] === $input['total']) {
            $content = '';
            for ($i = 1; $i <= $input['total']; ++$i) {
                $chunkFile = $chunkdir . $input['hash'] . '_' . $i . '.chunk';
                if (!file_exists($chunkFile)) {
                    tips('文件上传失败');
                }
                $content .= file_get_contents($chunkFile);
                unlink($chunkFile);
            }

            $mid = generate_id();
            // 原文件名
            $filename = $input['name'];
            // 文件后缀名
            $suffix = pathinfo($filename)['extension'];
            // 保存路径
            $path = $this->getPath($suffix, $mid);
            $this->filesystem->writeStream($path, $content);

            $res = $this->materialData($mid, $path, $suffix, $filename, $input['size']);
        }
        $res['chunk'] = $input['index'];
        return $res;
    }
    /**
     * 素材数据
     */
    protected function materialData($mid, $path, $suffix, $filename, $size): array
    {
        $type = self::classify($suffix);
        $url = $this->appDomain . $path;
        // 图片文件
        if (self::IMAGE == $type) {
            // 图片格式压缩
            if ('webp' != $suffix) {
                list($path, $resource) = self::webpConvert($path, $suffix);
                $stream = fopen(BASE_PATH . self::DS . $resource, "r+");
                $this->filesystem->writeStream($path, $stream);
                fclose($stream);
                unlink(BASE_PATH . self::DS . $resource);
            }
        }
        // oss
        if ($this->isOss($type)) {
            $url = $this->dispatch($path);
            unlink($this->localRoot . $path);
        }
        $material = [
            'material_id' => $mid,
            'storage_mode' => $this->storageMode,
            'origin_name' => $filename,
            'object_name' => basename($path),
            'type' => $type,
            'path' => $path,
            'url' => $url,
            'size' => round($size / self::MB, 2)
        ];
        return $this->saveMaterial($material);
    }
    /**
     * 保存路径以及文件名
     */
    protected function getPath(string $suffix, string $name): string
    {
        $path = '/storage' . self::DS . self::classify($suffix) . self::DS . date('Ymd');
        $filedir = $this->localRoot . $path;
        if (!file_exists($filedir) && !is_dir($filedir)) {
            mkdir($filedir, 0777, true);
        }
        return $path . self::DS . $name . '.' . $suffix;
    }
    /**
     * oss存储
     */
    protected function isOss(string $type): bool
    {
        if ($this->storageMode == 'local') {
            return false;
        }
        if (in_array($type, [self::IMAGE, self::VIDEO, self::AUDIO])) {
            return true;
        }
        return false;
    }
    /**
     * 文件上传oss服务
     */
    protected function dispatch(string $path): string
    {
        $localfile = $this->localRoot . $path;
        // 默认oss配置
        $storage = $this->config->get('file.storage')[$this->storageMode];
        // 上传oss
        $oss = $this->factory->get($this->storageMode);
        $steam = file_get_contents($localfile);
        $res = $oss->write($path, $steam);
        if ($res == null) {
            tips('文件上传失败');
        }
        unlink($localfile);
        return ($storage['domain'] ?? '') . $path;
    }
    /**
     * 保存上传文件信息
     */
    protected function saveMaterial(array $data): array
    {
        $auth = Context::get(AppEnum::AUTH_MARK);
        $data['user_mode'] = $auth['user_mode'] ?? AppEnum::AUTH_MODE;
        $data['user_id'] = $auth['uid'] ?? 0;
        $data['created_at'] = $data['updated_at'] = time();
        Db::table('system_material')->insert($data);
        return [
            'name' => $data['origin_name'],
            'path' => $data['path'],
            'url' => $data['url'],
        ];
    }
    /**
     * 转换成webp格式图片
     */
    protected function webpConvert(string $path, string $suffix): array
    {
        $filename = $this->localRoot . $path;
        $info = getimagesize($filename);
        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($filename);
        } else if ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($filename);
        } else if ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($filename);
        } else if ($info['mime'] == 'image/bmp') {
            $image = imagecreatefrombmp($filename);
        } else {
            tips('不支持该图片格式压缩');
        }
        if (!$image) {
            tips('文件读取失败');
        }
        $savePath = 'public/' . generate_id() . '.webp';
        imagewebp($image, $savePath, 36);
        imagedestroy($image);
        unlink($filename);
        $path = str_replace('.' . $suffix, '.webp', $path);
        return [$path, $savePath];
    }
}
