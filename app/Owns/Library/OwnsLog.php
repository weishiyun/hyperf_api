<?php

declare(strict_types=1);

namespace App\Owns\Library;

use App\Owns\Enum\AppEnum;
use Hyperf\Context\Context;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * 日志
 */
class OwnsLog
{
    #[Inject]
    protected RequestInterface $request;
    /**
     * debug日志
     * @param string $param 操作参数
     * @param string $message 错误信息
     * @param string $module
     */
    public function debug(string $param, string $message, string $module = ''): void
    {
        $this->write('debug', $param, $message, $module);
    }
    /**
     * info日志
     * @param string $param 操作参数
     * @param string $message 错误信息
     * @param string $module
     */
    public function info(string $param, string $message, string $module = ''): void
    {
        $this->write('info', $param, $message, $module);
    }
    /**
     * warning日志
     * @param string $param 操作参数
     * @param string $message 错误信息
     * @param string $remark
     */
    public function warning(string $param, string $message, string $module = ''): void
    {
        $this->write('warning', $param, $message, $module);
    }
    /**
     * error日志
     * @param string $param 操作参数
     * @param string $message 错误信息
     * @param string $module
     */
    public function error(string $param, string $message, string $module = ''): void
    {
        $this->write('error', $param, $message, $module);
    }
    /**
     * 写入日志
     */
    private function write($level, $param, $message, string $module): void
    {
        // 请求信息
        $route = $this->request->url() ?? '';
        if ($module) {
            $module = explode('/', $route)[0] ?? '';
        }
        // 用户信息
        $auth = Context::get(AppEnum::AUTH_MARK, []);

        Db::table('system_log_' . date('Ym'))->insert([
            'module' => $module,
            'level' => $level,
            'param' => $param,
            'message' => $message,
            'route' => $route,
            'method' => $this->request->getMethod() ?? '',
            'uid' => $auth['uid'] ?? '0',
            'mode' => $auth['mode'] ?? AppEnum::AUTH_MODE,
            'created_at' => now_date()
        ]);
    }
}
