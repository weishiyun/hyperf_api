<?php

declare(strict_types=1);

namespace App\Owns\Library;

use App\Model\System\UserRoleModel;
use App\Owns\Enum\AppEnum;
use Hyperf\Context\Context;

/**
 * 登录用户
 */
class OwnsAuth
{
    /**
     * 用户id
     */
    public static function uid(): string
    {
        return Context::get(AppEnum::AUTH_MARK, [])['uid'] ?? '0';
    }
    /**
     * 用户详情
     */
    public static function info(): array
    {
        return (array) Context::get(AppEnum::AUTH_MARK, []);
    }
    /**
     * 是否超级管理员
     */
    public static function isSuper(): bool
    {
        return in_array(1000, self::roleIds());
    }
    /**
     * 获取角色id
     */
    public static function roleIds(): array
    {
        return UserRoleModel::getUserRoleIdOrCode(self::uid(), 'role_id');
    }
    /**
     * 获取角色代码
     */
    public static function roleCode(): array
    {
        return UserRoleModel::getUserRoleIdOrCode(self::uid(), 'code');
    }
}