<?php

declare(strict_types=1);

namespace App\Owns\Aspect;

use App\Owns\Annotation\Auth;
use App\Owns\Enum\RespCode;
use App\Owns\Enum\AppEnum;
use Hyperf\Context\Context;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;

/**
 * Class AuthAspect.
 */
#[Aspect]
class AuthAspect extends AbstractAspect
{
    public array $annotations = [
        Auth::class,
    ];

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $user = Context::get(AppEnum::AUTH_MARK, []);
        if (!$user) {
            tips('身份校验失败，请重新登录', RespCode::SIGN_MISSING);
        }
        return $proceedingJoinPoint->process();
    }
}
