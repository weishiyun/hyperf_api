<?php

declare(strict_types=1);

namespace App\Owns\Aspect;

use App\Owns\Annotation\Oper;
use App\Owns\Enum\AppEnum;
use App\Owns\Helper\HttpRequest;
use Hyperf\Context\Context;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;

use function Hyperf\Support\make;

/**
 * Class OperAspect.
 */
#[Aspect]
class OperAspect extends AbstractAspect
{
    public array $annotations = [
        Oper::class,
    ];

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $result = $proceedingJoinPoint->process();
        $user = Context::get(AppEnum::AUTH_MARK, []);
        if ($user) {
            $this->writeLog($result);
        }
        return $result;
    }

    protected function writeLog($response)
    {
        $user = Context::get(AppEnum::AUTH_MARK, []);
        $http = make(HttpRequest::class);
        $log = [
            'uid' => $user['uid'] ?? '0',
            'user_mode' => AppEnum::AUTH_MODE,
            'param' => $http->getParam(),
            'response_code' => $response->getStatusCode(), 
            'response_data' => $response->getBody()->getContents(), 
            'ip' => $http->getIp(),
            'os' => $http->getOs(),
            'browser' => $http->getBrowser(),
            'created_at' => now_date(),
        ];
        [$log['module'], $log['route'], $log['method']] = $http->getRoute();
        Db::table('system_oper_log')->insert($log);
    }
}
