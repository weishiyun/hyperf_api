<?php

declare(strict_types=1);

namespace App\Owns\Aspect;

use App\Owns\Annotation\Record;
use App\Owns\Enum\AppEnum;
use App\Owns\Helper\HttpRequest;
use Hyperf\Context\Context;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;

use function Hyperf\Support\make;

/**
 * Class RecordAspect.
 */
#[Aspect]
class RecordAspect extends AbstractAspect
{
    public array $annotations = [
        Record::class,
    ];

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $result = $proceedingJoinPoint->process();
        $this->writeLog();
        return $result;
    }

    protected function writeLog()
    {
        $user = Context::get(AppEnum::AUTH_MARK, []);
        $http = make(HttpRequest::class);
        $log = [
            'uid' => $user['uid'] ?? '0',
            'user_mode' => AppEnum::AUTH_MODE,
            'param' => $http->getParam(),
            'ip' => $http->getIp(),
            'os' => $http->getOs(),
            'created_at' => now_date(),
        ];
        [$log['module'], $log['route'], $log['method']] = $http->getRoute();
        Db::table('system_api_log')->insert($log);
    }
}
