<?php

declare(strict_types=1);

namespace App\Owns\Middleware;

use App\Owns\Enum\AppEnum;
use App\Owns\Library\OwnsJwt;
use Hyperf\Context\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * 跨域
 */
class Core implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = Context::get(ResponseInterface::class);
        $response = $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
            ->withHeader('Access-Control-Allow-Credentials', 'false')
            // ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->withHeader('Access-Control-Allow-Headers', 'accept-language,authorization,lang,uid,token,Keep-Alive,User-Agent,Cache-Control,Content-Type')
            ->withHeader('Access-Control-Max-Age', '3600');
        // 设置上下文
        Context::set(ResponseInterface::class, $response);
        if ($request->getMethod() == 'OPTIONS') {
            return $response;
        }
        // 用户token
        $token = $request->getHeaders()['authorization'][0] ?? '';
        $user = $token ? OwnsJwt::instance()->checkToken($token) : [];
        Context::set(AppEnum::AUTH_MARK, $user);
        return $handler->handle($request);
    }
}
