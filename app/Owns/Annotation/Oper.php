<?php

declare(strict_types=1);

namespace App\Owns\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * 操作日志
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Oper extends AbstractAnnotation
{
    /**
     * @param string $code
     */
    public function __construct(public string $code = '')
    {
    }
}
