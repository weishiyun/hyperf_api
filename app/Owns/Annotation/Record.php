<?php

declare(strict_types=1);

namespace App\Owns\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * 接口访问记录
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Record extends AbstractAnnotation
{
    /**
     * @param string $route
     */
    public function __construct(public string $route = '')
    {
    }
}
