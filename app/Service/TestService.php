<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\System\UserModel;
use Hyperf\AsyncQueue\Annotation\AsyncQueueMessage;

/**
 * 测试
 */
class TestService
{
    #[AsyncQueueMessage('default', 5, 2)]
    public function test(array $param): void
    {
        $queueLog = logger('queue');
        $queueLog->info('日志：' . json_encode($param, JSON_UNESCAPED_UNICODE));
        
        var_dump($param);
    }

    public function user(): object
    {
        return UserModel::query()->select(['username', 'password', 'user_type', 'user_id'])->first();
    }
}
