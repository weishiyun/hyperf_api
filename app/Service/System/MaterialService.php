<?php

declare(strict_types=1);

namespace App\Service\System;

use App\Model\System\MaterialModel;
use App\Owns\Enum\AppEnum;
use App\Owns\Library\OwnsFile;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpMessage\Upload\UploadedFile;

/**
 * 素材文件
 */
class MaterialService
{
    #[Inject]
    protected OwnsFile $upload;
    /**
     * 上传文件
     * @param UploadedFile $file 上传文件
     */
    public function upload(UploadedFile $file): array
    {
        if(!$file->isValid()) {
            tips('无效文件，上传失败');
        }
        return $this->upload->handleUpload($file);
    }
    /**
     * 切片上传
     * @param UploadedFile $file
     * @param array $input
     */
    public function chunkUpload(UploadedFile $file, array $input): array
    {
        if(!$file->isValid()) {
            tips('无效文件，上传失败');
        }
        return $this->upload->handleChunkUpload($file, $input);
    }
    /**
     * 文件详情
     * @param string $material_id
     */
    public function info(string $material_id)
    {
        $info = MaterialModel::query()->select([
            'material_id', 'type', 'origin_name', 'path', 'url'
        ])->where('material_id', $material_id)
            ->where('status', AppEnum::ENABLE)
            ->where('deleted_at', AppEnum::UNDELETE)
            ->first();

        return $info;
    }
}
