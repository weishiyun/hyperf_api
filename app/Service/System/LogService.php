<?php

declare(strict_types=1);

namespace App\Service\System;

use App\Owns\Library\OwnsLog;
use Hyperf\Di\Annotation\Inject;

/**
 * 日志
 */
class LogService
{
    #[Inject]
    protected OwnsLog $log;

}
