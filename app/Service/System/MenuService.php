<?php

declare(strict_types=1);

namespace App\Service\System;

use App\Model\System\MenuModel;
use App\Model\System\RoleMenuModel;
use App\Owns\Enum\AppEnum;
use App\Owns\Library\OwnsAuth;
use Hyperf\DbConnection\Db;

/**
 * 菜单
 */
class MenuService
{
    public function treeList()
    {

    }
    /**
     * 角色菜单
     */
    public function getRoutersMenu(array $role_ids): array
    {
        $res = [];
        if(!$role_ids) {
            return $res;
        }
        $field = [
            'sM.menu_id', 'sM.parent_id', 'sM.name', 'sM.route', 'sM.component',
            'sM.redirect', 'sM.icon', 'sM.type', 'sM.is_hidden'
        ];
        $query = Db::table(MenuModel::TABME . ' as sM')
            ->whereIn('sM.type', ['M', 'L', 'I'])
            ->where('sM.status', AppEnum::ENABLE);
        if(!OwnsAuth::isSuper())  {
            $query->join(RoleMenuModel::TABME . ' as sRm', 'sRm.menu_id', 'sM.menu_id')
                ->whereIn('sRm.role_id', $role_ids);
        }
        $list = $query->select($field)
            ->orderBy('sM.sort', 'desc')
            ->get();
        foreach($list as $item) {
            $res[] = [
                'id' => $item->menu_id,
                'meta' => [
                    'hidden' => $item->is_hidden ? true : false,
                    'hiddenBreadcrumb' => $item->is_hidden ? true : false,
                    'icon' => $item->icon,
                    'title' => $item->name,
                    'type' => $item->type
                ],
                'name' => $item->name,
                'parent_id' => $item->parent_id,
                'path' => str_replace(':', '/', $item->route),
                'redirect' => $item->redirect
            ];
        }
        return recursion($res);
    }
}