<?php

declare(strict_types=1);

namespace App\Service\System;

use App\Model\System\UserModel;
use App\Owns\Enum\AppEnum;
use App\Owns\Library\OwnsAuth;
use App\Owns\Library\OwnsJwt;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;

/**
 * 登录
 */
class LoginService
{
    #[Inject]
    protected ConfigInterface $config;

    /**
     * 账号登录
     * @param array $input
     */
    public function account(array $input)
    {
        $user = UserModel::query()
            ->where(function ($query) use ($input) {
                $query->where('mobile', $input['username'])->orWhere('username', $input['username']);
            })
            ->where('deleted_at', 0)
            ->first();
        if (!$user) {
            tips('无账号信息');
        }
        if ($user->password != UserService::pwdEncryption($input['password'])) {
            tips('密码不正确');
        }
        if ($user->status != AppEnum::ENABLE) {
            tips('账号已被禁用');
        }
        $token = OwnsJwt::instance()->getToken([
            'uid' => $user->user_id,
            'user_type' => $user->user_type
        ]);
        return [
            'token' => $token,
            'ttl' => time() + $this->config->get('jwt.ttl') - 900
        ];
    }
    /**
     * 获取用户信息
     */
    public function info()
    {
        $field = [
            'user_id', 'username', 'nickname', 'mobile', 'email',
            'user_type', 'avatar', 'signed',
        ];
        $user = UserModel::query()->where('user_id', OwnsAuth::uid())
            ->select($field)
            ->first();

        return $user;
    }
    /**
     * 登出
     */
    public function logout(): bool
    {
        return OwnsJwt::instance()->logout(OwnsAuth::uid());
    }
    /**
     * 刷新token
     */
    public function refresh(string $token): string
    {
        return OwnsJwt::instance()->refreshToken($token);
    }
}
