<?php

declare(strict_types=1);

namespace App\Service\System;

use App\Model\System\UserModel;
use App\Owns\Enum\AppEnum;

/**
 * 管理员
 */
class UserService
{
    /**
     * 条件查询
     */
    protected static function where(object &$query, array $where)
    {
        $query->where('deleted_at', AppEnum::UNDELETE);
        // 状态
        $status = $where['status'] ?? '';
        if (is_numeric($status)) {
            $query->where('status', $status);
        }
        // 用户名
        if ($username = $where['username'] ?? '') {
            $query->where('username', 'like', '%' . $username . '%');
        }
        // 手机号
        if ($mobile = $where['mobile'] ?? '') {
            $query->where('mobile', 'like', '%' . $mobile . '%');
        }
    }
    /**
     * 列表
     */
    public function list(array $where)
    {
        $query = UserModel::query();

        self::where($query, $where);

        $field = [
            'user_id', 'username', 'nickname', 'avatar', 'mobile', 'email', 'user_type',
            'status', 'created_at'
        ];
        $total = $query->count();
        $list = $query->select($field)
            ->orderBy('created_at', 'desc')
            ->offset($where['offset'])
            ->limit($where['limit'])
            ->get();

        return list_format($list, $total);
    }
    /**
     * 编辑
     * @param array $input
     * @param string $user_id
     */
    public function edit(array $input, string $user_id = '')
    {
        if ($user_id) {
            $user = UserModel::query()
                ->where('user_id', $user_id)
                ->where('deleted_at', AppEnum::UNDELETE)
                ->first();
            if (!$user) {
                tips('无管理员信息');
            }
            UserModel::query()->where('user_id', $user_id)->update($input);
        } else {
            $input['password'] = self::pwdEncryption($input['password']);
            UserModel::query()->insert($input);
        }
    }
    /**
     * 加密密码
     * @param string $pwd
     */
    public static function pwdEncryption(string $pwd): string
    {
        return md5($pwd . AppEnum::SECRET);
    }

}
