<?php

declare(strict_types=1);

namespace App\Model\Setting;

use App\Owns\Abstract\OwnsModel;

class ConfigGroupModel extends OwnsModel
{
    protected ?string $table = 'setting_config_group';

    public const TABME = 'setting_config_group';

    protected array $casts = [
        'id' => 'string',
    ];
}
