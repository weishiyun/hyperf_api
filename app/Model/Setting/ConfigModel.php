<?php

declare(strict_types=1);

namespace App\Model\Setting;

use App\Owns\Abstract\OwnsModel;

class ConfigModel extends OwnsModel
{
    protected ?string $table = 'setting_config';

    protected string $primaryKey = 'key';

    public const TABME = 'setting_config';
}
