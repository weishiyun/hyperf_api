<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class UserModel extends OwnsModel
{
    protected ?string $table = 'system_user';

    protected string $primaryKey = 'user_id';

    public const TABME = 'system_user';

    protected array $casts = [
        'user_id' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
    ];
}
