<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class PostModel extends OwnsModel
{
    protected ?string $table = 'system_post';

    protected string $primaryKey = 'post_id';

    public const TABME = 'system_post';

    protected array $casts = [
        'post_id' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
    ];
}
