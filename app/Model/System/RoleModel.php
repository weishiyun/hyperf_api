<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class RoleModel extends OwnsModel
{
    protected ?string $table = 'system_role';

    protected string $primaryKey = 'role_id';

    public const TABME = 'system_role';

    protected array $casts = [
        'role_id' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
    ];
}
