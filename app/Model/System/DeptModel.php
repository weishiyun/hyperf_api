<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class DeptModel extends OwnsModel
{
    protected ?string $table = 'system_dept';

    protected string $primaryKey = 'dept_id';

    public const TABME = 'system_dept';

    protected array $casts = [
        'dept_id' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
    ];
}
