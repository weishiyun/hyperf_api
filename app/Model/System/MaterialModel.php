<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class MaterialModel extends OwnsModel
{
    protected ?string $table = 'system_material';

    protected string $primaryKey = 'material_id';

    public const TABME = 'system_material';

    protected array $casts = [
        'material_id' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
    ];
}
