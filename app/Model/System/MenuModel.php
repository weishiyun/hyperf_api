<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class MenuModel extends OwnsModel
{
    protected ?string $table = 'system_menu';

    protected string $primaryKey = 'menu_id';

    public const TABME = 'system_menu';

    protected array $casts = [
        'menu_id' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
    ];
}
