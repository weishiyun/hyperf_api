<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class DeptLeaderModel extends OwnsModel
{
    protected ?string $table = 'system_dept_leader';

    public const TABME = 'system_dept_leader';

}
