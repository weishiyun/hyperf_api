<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;
use App\Owns\Enum\AppEnum;
use Hyperf\DbConnection\Db;

class RoleMenuModel extends OwnsModel
{
    protected ?string $table = 'system_role_menu';

    public const TABME = 'system_role_menu';

    /**
     * 通过角色ID列表获取菜单ID
     */
    public static function getMenuIdsByRoleIds(array $role_ids): array
    {
        if(!$role_ids) {
            return [];
        }
        return Db::table(self::TABME . ' as sRm')
            ->join(MenuModel::TABME . ' as sM', 'sM.menu_id', 'sRm.menu_id')
            ->where('sM.status', AppEnum::ENABLE)
            ->pluck('sRm.menu_id')->toArray();
    }
}
