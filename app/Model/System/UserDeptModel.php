<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class UserDeptModel extends OwnsModel
{
    protected ?string $table = 'system_user_dept';

    public const TABME = 'system_user_dept';
}
