<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class RoleDeptModel extends OwnsModel
{
    protected ?string $table = 'system_role_dept';

    public const TABME = 'system_role_dept';
}
