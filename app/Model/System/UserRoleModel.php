<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;
use App\Owns\Enum\AppEnum;
use Hyperf\DbConnection\Db;

class UserRoleModel extends OwnsModel
{
    protected ?string $table = 'system_user_role';

    public const TABME = 'system_user_role';

    /**
     * 获取用户角色id或者code
     * @param string $uid
     */
    public static function getUserRoleIdOrCode(string $uid, string $field): array
    {
        if(!$uid) {
            return [];
        }
        return Db::table(self::TABME . ' as sUr')
            ->join(RoleModel::TABME . ' as sR', 'sR.role_id', 'sUr.role_id')
            ->where('sUr.user_id', $uid)
            ->where('sR.status', AppEnum::ENABLE)
            ->where('sR.deleted_at', AppEnum::UNDELETE)
            ->pluck('sR.' . $field)->toArray();
    }
}
