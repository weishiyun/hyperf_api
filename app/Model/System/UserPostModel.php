<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class UserPostModel extends OwnsModel
{
    protected ?string $table = 'system_user_post';

    public const TABME = 'system_user_post';
}
