<?php

declare(strict_types=1);

namespace App\Model\System;

use App\Owns\Abstract\OwnsModel;

class LogModel extends OwnsModel
{
    protected ?string $table = 'system_log';

    public const TABME = 'system_log';

}
